# fiou

[![License](https://is.gd/GbWFMI)](https://opensource.org/licenses/ISC)

**f**ile.**io** **u**ploader 📑 🡆 ☁️

A command line utility for using [www.file.io](https://www.file.io/); the *ephemeral* file sharing site. Encrypt and upload your file with one easy command from the comfort of your favourite shell!

### Dependencies
+ [curl](https://curl.haxx.se/)
+ [make](https://askubuntu.com/questions/161104/how-do-i-install-make)
+ [jq](https://stedolan.github.io/jq/)
+ [xpg](https://gitlab.com/klosure/xpg) (*optional*)
+ [qe](https://gitlab.com/klosure/qe) (*optional*)

> `fiou` is compatible with any POSIX shell, *i.e.* bash, zsh, ksh, etc.

### Installation
```bash
git clone https://gitlab.com/klosure/fiou.git
cd fiou
make deps

# as root
make install

# to uninstall
make uninstall
```

### Usage

```bash
  fiou to_share.txt                  # upload to_share.txt
  fiou -e ./workdir/                 # encrypt workdir and upload*
  fiou -a song.mp3                   # get pass from xpg, encrypt file, and upload**
  fiou -x 4m secret.txt              # upload to_share.txt, expires after 4 months
  fiou -x 1y -e temp\ dir/           # encrypt temp\ dir and upload, setting expiration to one year
  fiou -x 12w -a song.mp3            # get pass from xpg, encrypt file, then upload, setting expiration to twelve weeks
  fiou -o                            # output your settings from ~/.fiourc
  fiou -i to_share.txt               # ignore the ~/.fiourc settings file (use defaults)
  fiou -h                            # show this help message
  fiou -v                            # show the version info
    
  (-e flag requires the qe application *)
  (-a flag requires the xpg and qe application **)
  (both of these applications can be found at https://git.zerohack.xyz/lotus)
```

#### Video Example

[![asciicast](https://asciinema.org/a/305284.svg)](https://asciinema.org/a/305284)

> `fiou` will tar and gzip directories by default prior to upload. If you install the optional dependency `qe`, it can handle more customizable settings like changing the compression algorithm and flags (as well as encryption).

### Configuration
If you would like to set the default behavior of fiou, it looks for a configuration file in your home directory `~/.fiourc`. This will help you by allowing you to skip entering flags manually.

### Notes
While this tool will work fine without installing `xpg` or `qe`, I've included them because it streamlines the process of protecting your file uploads. If you have a lot of files it is a pain to encrypt each one by hand before uploading it. There is a **5GB limit** to what you can upload; `fiou` will check before beginning the upload to make sure we don't exceed this limit. If you are close to this limit it would be advisable to compress your file prior to calling `fiou` to make it past this size check successfully.

### License / Disclaimer
This project is licensed under the [ISC license](LICENSE.md). I am not associated with the *file.io* team or *introvert.com* in any capacity. I cannot guarantee against any potential problems with using this service or tool, including site uptime. I would recommend having a backup of whatever files you upload to this service in order to prevent data loss.
