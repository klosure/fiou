#!/bin/sh
#
# author: klosure
# https://gitlab.com/klosure/fiou
# license: isc
#
# --- file.io upload utility ---
# This tool allows us to easily upload a file to the www.file.io website
# using one simple command. If you have the optional dependencies installed
# it can also handle encrypting and tarring your file/dir, for you prior to upload.
# 
# File size upload limit = 5 GB
#


VERSION=0.1.2

# --- GLOBAL VARS ---
EXPIRY="" # default expiration duration
E_FLAG="" # encrypt only
A_FLAG="" # generate password and encrypt
# these helper values are used to check the range of expiration passed to us
MAX_DAYS=3520
MAX_WEEKS=520
MAX_MONTHS=120
MAX_YEARS=10


# setup global vars to their default values
fun_set_global_var_defaults () {
    EXPIRY="14d"
    E_FLAG="false"
    A_FLAG="false"
}


# called on ctrl-c press
fun_cleanup () {
    # this doesn't really do anything yet. For future use..
    echo "fiou: caught sigint, cleaning up..."
    exit 0
}


# called when an error occurred
# $1 is the message to be printed
# then cleanup and exit out!
fun_error () {
    echo "$1"
    echo ""
    fun_print_usage
    exit 1
}


# this function is called when we are clear to
# begin uploading our fully processed file
fun_upload () {
    U_TARGET="$1"
    echo "Uploading: $U_TARGET"
    RESPONSE=$(curl --progress-bar -F "file=@$U_TARGET" https://file.io/?expires="$EXPIRY")
    SUCCESS=$(echo "$RESPONSE" | jq .success)
    if [ "$SUCCESS" = "true" ] ; then
	# we got a good json response back
	LINK=$(echo "$RESPONSE" | jq .link)
	# strip off the quotes
	UNQUOTE_LNK="${LINK%\"}"
	UNQUOTE_LNK="${UNQUOTE_LNK#\"}"
	echo "Expires in: $EXPIRY"
	echo "Link: $UNQUOTE_LNK"
	unset LINK UNQUOTE_LNK # i like turtles
    else
	fun_error "bad json response from file.io, is there a issue with your network?"
    fi
    unset U_TARGET SUCCESS
}


# our target is a directory, so we need to archive it.
fun_tar_dir () {
    TAR_INPUT="$1"
    # only tar, no compression
    TAR_OUTPUT="$TAR_INPUT".tar.gz
    echo "Creating tarball: $TAR_OUTPUT"
    tar -czf "$TAR_OUTPUT" "$TAR_INPUT" 
    unset TAR_INPUT TAR_OUTPUT
}


# is the filename that was passed in actually a directory?
fun_is_dir () {
    if [ -d "$1" ] ; then
	return 0 # true
    else
	if [ -s "$1" ] ; then
	    return 1 # false
	else
	    # bad filename was passed in
	    fun_error "error: bad filename given!"
	fi
    fi
}


# run any encryption phases
fun_preprocess () {
    PP_TARGET="$1"
    if [ "$A_FLAG" = "true" ] ; then
	# -a flag was passed in
	echo "Encrypting: $PP_TARGET"
	PASS=$(xpg)
	echo "Using Password: $PASS"
        printf '%s\n' "$PASS" "$PASS" | script -O /dev/null -q -c "qe $PP_TARGET" > /dev/null
	BASE_PP_TARGET=$(basename "$PP_TARGET")
	NEW_TARGET=$(ls "$BASE_PP_TARGET"*.gpg)
	unset BASE_PP_TARGET
    elif [ "$E_FLAG" = "true" ] ; then
	# -e flag was passed in
	echo "Encrypting: $PP_TARGET"
	qe "$PP_TARGET"
	BASE_PP_TARGET=$(basename "$PP_TARGET")
	NEW_TARGET=$(ls "$BASE_PP_TARGET"*.gpg)
	unset BASE_PP_TARGET
    else
        # filename didn't change since we did no encryption
	NEW_TARGET="$PP_TARGET"
    fi
    if fun_is_dir "$NEW_TARGET" ; then
	# if we still have a dir here we have to tar it before upload
	NEW_TARGET=$(basename "$NEW_TARGET")
	fun_tar_dir "$NEW_TARGET"
	NEW_TARGET="$NEW_TARGET.tar.gz"
    fi
    fun_upload "$NEW_TARGET"
    unset PP_TARGET NEW_TARGET
}


# make sure that our target file is under 5GB
fun_check_size () {
    S_TARGET="$*"
    SIZE=$(du -s --apparent-size "$S_TARGET" | cut -f 1)
    if [ "$SIZE" -lt 5000000 ] ; then
	# yay! our file size small enough
	fun_preprocess "$S_TARGET"
    else
	echo "file size = $(du -h "$S_TARGET")"
	unset S_TARGET
	fun_error "The file was too close to the size limit of 5GB. Compress the file and try again!"
    fi
    unset S_TARGET
}


# print out the help info on how to run the program
fun_print_usage () {
    echo "usage: fiou to_share.txt                  # upload to_share.txt"
    echo "       fiou -x 4m secret.txt              # upload to_share.txt, expires after 4 months"
    echo "       fiou -e ./workdir/                 # encrypt workdir and upload"
    echo "       fiou -x 1y -e temp\ dir/           # encrypt temp\ dir and upload, setting expiration to one year"
    echo "       fiou -a song.mp3                   # get pass from xpg, then encrypt the file and upload"
    echo "       fiou -x 12w -a song.mp3            # get pass from xpg, then encrypt file, then upload, setting expiration to twelve weeks"
    echo "       fiou -h                            # show this help message"
    echo "       fiou -v                            # show the version info"
    echo "       fiou -o                            # output your settings from ~/.fiourc"
    echo "       fiou -i to_share.txt               # ignore the ~/.fiourc settings file (use defaults)"
    echo ""
    echo "(-e flag requires the qe application)"
    echo "(-a flag requires the xpg and qe application)"
    echo "(both of these applications can be found at https://git.sr.ht/~voidraven/fiou)"
}


# will be called when run with the -v flag
fun_print_ver () {
    echo "version: $VERSION"
}


# show the user our program, license, and author info
fun_print_info () {
    echo "fiou - a file upload utility for www.file.io"
    echo "author: klosure"
    echo "site: https://gitlab.com/klosure/fiou"
    echo "license: isc"
    fun_print_ver
    echo ""
    fun_print_usage
}


# this function is called when the user passes the -e flag
# check that qe is installed, set the global flag, and shift it out of the arg list
fun_set_e () {
    if [ "$A_FLAG" = "false" ] ; then
	if [ -x "$(command -v qe)" ]; then
	    E_FLAG="true"
	else
	    fun_error "error: for fiou to use the -e flag qe must be installed"
	fi
    else
	fun_error "-a flag was already set. These settings are mutually exclusive."
    fi
}


# this function is called when the user passes the -a flag
# check that xpg & qe is installed, set the global flag, and shift it out of the arg list
fun_set_a () {
    if [ "$E_FLAG" = "false" ] ; then
	if [ -x "$(command -v xpg)" ]; then
	    if [ -x "$(command -v qe)" ]; then
		A_FLAG="true"
	    else
		fun_error "error: for fiou to use the -a flag qe must be installed"
	    fi
	else
	    fun_error "error: for fiou to use the -a flag xpg must be installed"
	fi
    else
	fun_error "error: -e flag was already set! you can't use both -e and -a. pick one or the other."
    fi
}


# make sure that our core dependencies are installed
fun_check_deps () {
    if ! [ -x "$(command -v curl)" ]; then
	fun_error "error: fiou needs curl, please install it."
    elif ! [ -x "$(command -v jq)" ]; then
	fun_error "error: fiou needs jq, please install it."
    fi
}


# make sure we were given sane ranges for the expiry
# the maximum values are set at the top
fun_range_check () {
    LEN="$1"
    UNITS="$2"
    if [ "$UNITS" = "d" ] ; then
	if [ "$LEN" -gt 0 ] && [ "$LEN" -lt "$MAX_DAYS" ] ; then
	    EXPIRY="$LEN$UNITS"
	else
	    fun_error "the expiration provided was too big/small"
	fi
    elif [ "$UNITS" = "w" ] ; then
	if [ "$LEN" -gt 0 ] && [ "$LEN" -lt "$MAX_WEEKS" ] ; then
	    EXPIRY="$LEN$UNITS"
	else
	    fun_error "the expiration provided was too big/small"
	fi
    elif [ "$UNITS" = "m" ] ; then
	if [ "$LEN" -gt 0 ] && [ "$LEN" -lt "$MAX_MONTHS" ] ; then
	    EXPIRY="$LEN$UNITS"
	else
	    fun_error "the expiration provided was too big/small"
	fi
    else # must be years
	if [ "$LEN" -gt 0 ] && [ "$LEN" -lt "$MAX_YEARS" ] ; then
	    EXPIRY="$LEN$UNITS"
	else
	    fun_error "the expiration provided was too big/small"
	fi
    fi
}


# The -x flag was passed in
# make sure that we were given a valid option
fun_set_exp () {
    EXP_VAL="$2"
    UNIT_CHAR=$(printf "%s" "$EXP_VAL" | tail -c 1)
    # check the units
    if [ "$UNIT_CHAR" = "d" ] || [ "$UNIT_CHAR" = "w" ] || [ "$UNIT_CHAR" = "m" ] || [ "$UNIT_CHAR" = "y" ] ; then   
	DIGITS=$(printf "%s" "$EXP_VAL" | rev | cut -c 2- | rev)
	# make sure that digits contains only numbers
	# if it looks ok, go ahead and check the range of the number
        case "$DIGITS" in
	    ''|*[!0-9]*) fun_error "bad expiration value!" ;;
	    *) fun_range_check "$DIGITS" "$UNIT_CHAR" ;;
	esac
    else
	fun_error "that was not a recognized unit of time, use d, w, m, y"
    fi
}


# parse what arguments were passed in
# call a corresponding function
fun_check_args () {
    if [ "$#" -gt 0 ] ; then
	case "$1" in
            -h)	 fun_print_info "$@" ;;
            -v)  fun_print_ver "$@" ;;
	    -i)  shift 1 && fun_set_global_var_defaults && fun_check_args "$@" ;;
	    -o)  fun_output_settings ;;
	    -e)  shift 1 && fun_set_e && fun_check_args "$@" ;;
	    -a)  shift 1 && fun_set_a && fun_check_args "$@" ;;
	    -x)  fun_set_exp "$@" && shift 2 && fun_check_args "$@" ;;
            *)	 fun_check_size "$@" ;;
	esac
    else
	fun_error "Pass in a filename or flag!"
    fi
}


# (see the included sample-fiou for an example!)
fun_check_rc () {
    if [ -s ~/.fiourc ] ; then
	# shellcheck source=/dev/null
	. ~/.fiourc # the values in here will override the defaults
	if [ "$A_FLAG" = true ] && [ "$E_FLAG" = "true" ] ; then
	    fun_error "Error parsing .fiourc A_FLAG and E_FLAG cannot both equal true."
	fi
    fi
}


# print out the settings in the fiou rc file
fun_output_settings () {
    if ! [ -s ~/.fiourc ] ; then
	echo ".fiourc not found, using defaults values (Did you forget to run 'make deps')"
    fi
    echo "Expiration (-x): $EXPIRY"
    echo "Encrypt only (-e): $E_FLAG"
    echo "Generate Password AND encrypt (-a): $A_FLAG"
    exit 0
}

# --- ENTRY POINT ---
trap 'fun_cleanup' INT # catch a ctrl-c
fun_check_deps
fun_set_global_var_defaults
fun_check_rc
fun_check_args "$@"
exit 0 # see you next time!
