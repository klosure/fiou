PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man/man1

.POSIX: install

all: help

deps:
	cp sample-fiourc ~/.fiourc
help:
	@echo "please run 'make install' as root"
install:
	cp ./fiou.sh $(BINDIR)/fiou
	cp ./fiou.1 $(MANDIR)
uninstall:
	rm $(BINDIR)/fiou
	rm $(MANDIR)/fiou.1
	rm ~/.fiourc
test:
	shellcheck fiou.sh
